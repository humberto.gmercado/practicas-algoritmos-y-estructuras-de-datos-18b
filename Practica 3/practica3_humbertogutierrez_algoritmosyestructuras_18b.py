"""	Practica 3
Utilice funciones, paso por parametros y por referencia.
La clínica de salud “San Juan XXIII”, necesita registrar todos los movimientos que lleva a cabo en sus operaciones, 
por lo cual le solicitan realizar el guardado de la información de los siguientes puntos.

·         Administrativos
·         Enfermeros
·         Doctores
·         Pacientes
·         Autos compactos
·         Ambulancias
·         Tractocamiones
·         Camionetas
·         Motocicletas

Con la necesidad que se presenta, elabore dos estructuras bases, una para poder gestionar a todas las personas y otra para gestionar los vehículos, 
tomando en cuenta los siguientes datos como base y adicione los faltantes.

Persona
·         Nombre
·         Apellido paterno
·         Apellido materno
·         Fecha de nacimiento
·         Edad (se obtiene automáticamente)
·         Hora de alta (se obtiene automáticamente)
·         Curp
·         Rfc
·         Tipo de sangre
·         Padecimientos
·         Consume estupefacientes
·         Enfermedades crónicas degenerativas
·         Descripción de enfermedades crónicas degenerativas
Vehiculos
·         Tipo
·         Marca
·         Modelo
·         Rodado
·         Numero de llantas
·         Transmisión
·         Cilindraje
·         Numero puertas
·         Asignado a

Elabore un menú en el cual pueda realizar las siguientes operaciones para cada caso

·         Alta
·         Búsqueda por clave o placas según el caso (Solo muestra los datos)
·         Modificar datos por clave o placas según el caso
·         Eliminar por clave o placas según el caso
·         Mostrar todos los empleados entre 30 y 40 años

Todos los datos se deben de guardar en la memoria volátil de la computadora y en un archivo, como un valor separado por comas, 
por lo cual deberá existir un archivo para cada opción que se gestiona, esto asegura que, al apagarse el equipo, usted puede 
consultar la información, todos los valores deben ser guardados como CSV. """

from io import open
import os
import csv
import time

def alta(pv, t):
	alt=[]
	dato="NULL"

	print("\n BIENVENIDO AL MENU DE ALTA\n Si no conoce algun dato, escriba NULL\n")
	if pv == 0:
		dato=input(" Ingrese su CURP: ")
		alt.append(dato)
		dato=input(" Ingrese su nombre: ")
		alt.append(dato)
		dato=input(" Ingrese su apellido paterno: ")
		alt.append(dato)
		dato=input(" Ingrese su apellido materno: ")
		alt.append(dato)
		dato=input(" Ingrese su fecha de nacimiento AAAAMMDD (Ejemplo: 19981112): ")
		alt.append(dato)
		datNum=int(dato)
		edad=2018-(datNum//10000)
		alt.append(str(edad))
		dato=input(" Ingrese su RFC: ")
		alt.append(dato)
		dato=input(" Ingrese su tipo de sangre: ")
		alt.append(dato)
		dato=input(" Consume estupefacientes? (Si // No): ")
		alt.append(dato)
		dato=input(" Padece de enfermedad crónica? (Si // No): ")
		alt.append(dato)
		if dato == 'Si':
			dato=input(" Ingrese el nombre de la enfermedad: ")
			alt.append(dato)
		else:
			alt.append("N/A")
		horaAlta=time.strftime("%H:%M")
		alt.append(horaAlta)
		diaAlta=time.strftime("%d/%m/%y")
		alt.append(str(diaAlta))
		newAlta=str(alt)
		if t == 0:
			tipo="pacientes"
			guardar(tipo,alt,pv)
		elif t == 1:
			tipo="administrativo"
			guardar(tipo,alt,pv)
		elif t == 2:
			tipo="doctor"
			guardar(tipo,alt,pv)
		elif t == 3:
			tipo="enfermero"
			guardar(tipo,alt,pv)
	elif pv == 1:
		dato=input(" Ingrese la matricula: ")
		alt.append(dato)
		dato=input(" Ingrese la marca: ")
		alt.append(dato)
		dato=input(" Ingrese el modelo: ")
		alt.append(dato)
		dato=input(" Ingrese el color: ")
		alt.append(dato)
		dato=input(" Ingrese el numero de llantas: ")
		alt.append(dato)
		dato=input(" Ingrese la transmisión: ")
		alt.append(dato)
		dato=input(" Ingrese el cilindraje: ")
		alt.append(dato)
		dato=input(" Ingrese el numero de puertas: ")
		alt.append(dato)
		dato=input(" Asignado a: ")
		alt.append(dato)
		horaAlta=time.strftime("%H:%M")
		alt.append(horaAlta)
		diaAlta=time.strftime("%d/%m/%y")
		alt.append(str(diaAlta))
		newAlta=str(alt)
		if t == 0:
			tipo="autocompacto"
			guardar(tipo,alt,pv)
		elif t == 1:
			tipo="ambulancia"
			guardar(tipo,alt,pv)
		elif t == 2:
			tipo="tractocamion"
			guardar(tipo,alt,pv)
		elif t == 3:
			tipo="camioneta"
			guardar(tipo,alt,pv)
		elif t == 4:
			tipo="motocicleta"
			guardar(tipo,alt,pv)
	print("\n Registro completo")
	input()
	clear()	

def busqueda(pv,t):
	columna=[]
	print("\n BIENVENIDO AL MENU DE ALTA\n")
	if t == 0:
		if pv == 0:
			nombre="pacientes.csv"
		elif pv == 1:
			nombre="autocompacto.csv"
	elif t == 1:
		if pv == 0:
			nombre="administrativo.csv"
		elif pv == 1:
			nombre="ambulancia.csv"
	elif t == 2:
		if pv == 0:
			nombre="doctor.csv"
		elif pv == 1:
			nombre="tractocamion.csv"
	elif t == 3:
		if pv == 0:
			nombre="enfermero.csv"
		elif pv == 1:
			nombre="camioneta.csv"
	elif t == 4:
		nombre="motocicleta.csv"
	archivo=open(nombre,"r")
	filas=archivo.readlines()
	archivo.seek(0)
	for lineas in archivo:
		dat=str(lineas.split(',')[0])
		columna.append(dat)
	archivo.close()
	datBus=input(" Introduce la clave de identificacion (CURP // Matricula): ")
	for x in range(0,len(columna)):
		if datBus == columna[x]:
			print(filas[x])
			break

def modificar(pv,t):
	columna=[]
	print("\n BIENVENIDO AL MENU DE MODIFICACION\n")
	if t == 0:
		if pv == 0:
			nombre="pacientes.csv"
		elif pv == 1:
			nombre="autocompacto.csv"
	elif t == 1:
		if pv == 0:
			nombre="administrativo.csv"
		elif pv == 1:
			nombre="ambulancia.csv"
	elif t == 2:
		if pv == 0:
			nombre="doctor.csv"
		elif pv == 1:
			nombre="tractocamion.csv"
	elif t == 3:
		if pv == 0:
			nombre="enfermero.csv"
		elif pv == 1:
			nombre="camioneta.csv"
	elif t == 4:
		nombre="motocicleta.csv"

	archivo=open(nombre,"w")
	filas=archivo.readlines()
	archivo.seek(0)
	for lineas in archivo:
		dat=str(lineas.split(',')[0])
		columna.append(dat)
	datBus=input(" Introduce la clave de identificacion (CURP // Matricula): ")
	for x in range(0,len(columna)):
		if datBus == columna[x]:
			print(filas[x])
			op=input(" Esta seguro de modificar este registro? (Si // No): ")
			if op == "Si":
				print("Eliminando")
			elif op == "No":
				print(" No se realizaron cambios dentro del archivo.")
				input()
				clear()
			break
	archivo.close()

def eliminar(pv,t):
	columna=[]
	print("\n BIENVENIDO AL MENU DE ELIMINACION\n")
	if t == 0:
		if pv == 0:
			nombre="pacientes.csv"
		elif pv == 1:
			nombre="autocompacto.csv"
	elif t == 1:
		if pv == 0:
			nombre="administrativo.csv"
		elif pv == 1:
			nombre="ambulancia.csv"
	elif t == 2:
		if pv == 0:
			nombre="doctor.csv"
		elif pv == 1:
			nombre="tractocamion.csv"
	elif t == 3:
		if pv == 0:
			nombre="enfermero.csv"
		elif pv == 1:
			nombre="camioneta.csv"
	elif t == 4:
		nombre="motocicleta.csv"
	archivo=open(nombre,"r")
	filas=archivo.readlines()
	archivo.seek(0)
	for lineas in archivo:
		dat=str(lineas.split(',')[0])
		columna.append(dat)
	datBus=input(" Introduce la clave de identificacion (CURP // Matricula): ")
	for x in range(0,100):
		if datBus == columna[x]:
			print(filas[x])
			op=input(" Esta seguro de eliminar este registro? (Si // No): ")
			if op == "Si":
				print("Eliminando")
				input()
			elif op == "No":
				print(" No se realizaron cambios dentro del archivo.")
				input()
				clear()
			break
	archivo.close()

def mostrar(t):
	columna=[]
	print("\n EMPLEADOS ENTRE 30 - 40\n")
	if t == 1:
			nombre="administrativo.csv"
	elif t == 2:
			nombre="doctor.csv"
	elif t == 3:
			nombre="enfermero.csv"

	archivo=open(nombre,"r")
	filas=archivo.readlines()
	archivo.seek(0)
	for lineas in archivo:
		dat=str(lineas.split(',')[5])
		columna.append(int(dat))
	for x in range(0,len(columna)):
		if columna[x] <= 40 and columna[x] >= 30:
			print(filas[x])
	archivo.close()

def guardar(tipo,datos,pv):
	if pv == 0:
		nombre=str(tipo)+".csv"
		archivo=open(nombre,"a")
		for x in range (0, 13):
			archivo.write(datos[x])
			archivo.write(",")
		archivo.write("\n")
		archivo.close()
	if pv == 1:
		nombre=str(tipo)+".csv"
		archivo=open(nombre,"a")
		for x in range (0, 11):
			archivo.write(datos[x])
			archivo.write(",")
		archivo.write("\n")
		archivo.close()
	print(" Guardado con exito.")
	input()
	clear()

def clear():
	os.system('cls')

print(" H0606 ALGORITMOS Y ESTRUCTURAS DE DATOS\n HUMBERTO ALEJANDRO GUTIERREZ MERCADO\tCODIGO: 214700153\n")

op=0
while op!=5:
	print("\tClínica de Salud “San Juan XXIII”")
	print("\n Menu:\n\t 0 - Alta.\n\t 1 - Busqueda.\n\t 2 - Modificar datos.\n\t 3 - Eliminar datos.\n\t 4 - Mostrar empleados entre la edad de [30 - 40].\n\t 5 - Salir.\n")
	op=int(input(" Seleccione una opción: "))

	if op == 0:
		pv=0
		while pv != 2:
			print("\n\t 0 - Persona\n\t 1 - Vehiculo\n\t 2 - Regresar\n")
			pv=int(input(" Seleccione una opción: "))
			if pv == 0:
				print("\n\t 0 - Paciente\n\t 1 - Administrativo\n\t 2 - Doctor\n\t 3 - Enfermero\n")
				t=int(input(" Elija la opcion que desea dar de alta: "))
				alta(pv,t)
				break
			elif pv == 1:
				print("\n\t 0 - Autos compactos\n\t 1 - Ambulancias\n\t 2 - Tractocamiones\n\t 3 - Camionetas\n\t 4 - Motocicletas\n")
				t=int(input(" Elija la opcion que desea dar de alta: "))
				alta(pv,t)
				break
			elif pv == 2:
				break
			else:
				print(" Opcion incorrecta, por favor ingrese una del menu.")
				input()
				clear()		
	elif op == 1:
		pv=0
		while pv != 2:
			print("\n\t 0 - Persona\n\t 1 - Vehiculo\n\t 2 - Regresar\n")
			pv=int(input(" Seleccione una opción: "))
			if pv == 0:
				print("\n\t 0 - Paciente\n\t 1 - Administrativo\n\t 2 - Doctor\n\t 3 - Enfermero\n")
				t=int(input(" Elija la opcion que desea buscar: "))
				busqueda(pv,t)
				break
			elif pv == 1:
				print("\n\t 0 - Autos compactos\n\t 1 - Ambulancias\n\t 2 - Tractocamiones\n\t 3 - Camionetas\n\t 4 - Motocicletas\n")
				t=int(input(" Elija la opcion que desea buscar: "))
				busqueda(pv,t)
				break
			elif pv == 2:
				break
			else:
				print(" Opcion incorrecta, por favor ingrese una del menu.")
				input()
				clear()
	elif op == 2:
		pv=0
		while pv != 2:
			print("\n\t 0 - Persona\n\t 1 - Vehiculo\n\t 2 - Regresar\n")
			pv=int(input(" Seleccione una opción: "))
			if pv == 0:
				print("\n\t 0 - Paciente\n\t 1 - Administrativo\n\t 2 - Doctor\n\t 3 - Enfermero\n")
				t=int(input(" Elija la opcion que desea modificar: "))
				modificar(pv,t)
				break
			elif pv == 1:
				print("\n\t 0 - Autos compactos\n\t 1 - Ambulancias\n\t 2 - Tractocamiones\n\t 3 - Camionetas\n\t 4 - Motocicletas\n")
				t=int(input(" Elija la opcion que desea modificar: "))
				modificar(pv,t)
				break
			elif pv == 2:
				break
			else:
				print(" Opcion incorrecta, por favor ingrese una del menu.")
				input()
				clear()
	elif op == 3:
		pv=0
		while pv != 2:
			print("\n\t 0 - Persona\n\t 1 - Vehiculo\n\t 2 - Regresar\n")
			pv=int(input(" Seleccione una opción: "))
			if pv == 0:
				print("\n\t 0 - Paciente\n\t 1 - Administrativo\n\t 2 - Doctor\n\t 3 - Enfermero\n")
				t=int(input(" Elija la opcion que desea eliminar: "))
				eliminar(pv,t)
				break
			elif pv == 1:
				print("\n\t 0 - Autos compactos\n\t 1 - Ambulancias\n\t 2 - Tractocamiones\n\t 3 - Camionetas\n\t 4 - Motocicletas\n")
				t=int(input(" Elija la opcion que desea eliminar: "))
				eliminar(pv,t)
				break
			elif pv == 2:
				break
			else:
				print(" Opcion incorrecta, por favor ingrese una del menu.")
				input()
				clear()
	elif op == 4:
		print("\n\t 1 - Administrativo\n\t 2 - Doctor\n\t 3 - Enfermero\n")
		t=int(input(" Elija la opcion que desea mostrar: "))
		mostrar(t)
		input()
		clear()
	elif op == 5:
		break
	else:
		print(" Opcion incorrecta, por favor ingrese una del menu.")
		input()
		clear()

print(" Gracias por utilizar nuestro servicio, vuelva pronto.")
input()