/* 
	Practica 3
Utilice funciones, paso por parametros y por referencia.
La cl�nica de salud �San Juan XXIII�, necesita registrar todos los movimientos que lleva a cabo en sus operaciones, 
por lo cual le solicitan realizar el guardado de la informaci�n de los siguientes puntos.

�         Administrativos
�         Enfermeros
�         Doctores
�         Pacientes
�         Autos compactos
�         Ambulancias
�         Tractocamiones
�         Camionetas
�         Motocicletas

Con la necesidad que se presenta, elabore dos estructuras bases, una para poder gestionar a todas las personas y otra para gestionar los veh�culos, 
tomando en cuenta los siguientes datos como base y adicione los faltantes.

Persona
�         Nombre
�         Apellido paterno
�         Apellido materno
�         Fecha de nacimiento
�         Edad (se obtiene autom�ticamente)
�         Hora de alta (se obtiene autom�ticamente)
�         Curp
�         Rfc
�         Tipo de sangre
�         Padecimientos
�         Consume estupefacientes
�         Enfermedades cr�nicas degenerativas
�         Descripci�n de enfermedades cr�nicas degenerativas
Vehiculos
�         Tipo
�         Marca
�         Modelo
�         Rodado
�         Numero de llantas
�         Transmisi�n
�         Cilindraje
�         Numero puertas
�         Asignado a

Elabore un men� en el cual pueda realizar las siguientes operaciones para cada caso

�         Alta
�         B�squeda por clave o placas seg�n el caso (Solo muestra los datos)
�         Modificar datos por clave o placas seg�n el caso
�         Eliminar por clave o placas seg�n el caso
�         Mostrar todos los empleados entre 30 y 40 a�os

Todos los datos se deben de guardar en la memoria vol�til de la computadora y en un archivo, como un valor separado por comas, 
por lo cual deber� existir un archivo para cada opci�n que se gestiona, esto asegura que, al apagarse el equipo, usted puede 
consultar la informaci�n, todos los valores deben ser guardados como CSV.
*/

#include <iostream>
#include <stdlib.h>
using namespace std;

struct persona{
	string nombre;
	string apellPat;
	string apellMat;
	string fechaNac;
	string edad;
	string fechaA;
	string curp;
	string rfc;
	string sangre;
	string padec;
	string enferm;
	string descrip;
	string area;
	string espec;
	string consul;
	string areaEnfer;
};

struct vehiculo{
	string tipo;
	string marca;
	string modelo;
	string rodado;
	string noLlantas;
	string trans;
	string cilind;
	string noPuertas;
	string asign;
};

persona listaP[100];
vehiculo listaV[100];

void alta(int d);
void modif(int d);
void mostrar(int d);
int busq(string c, int d);

main(){
	int d=0, pv=0, t=0 menu,s;
    string cod;
	
	cout<<" H0606 ALGORITMOS Y ESTRUCTURAS DE DATOS\n HUMBERTO ALEJANDRO GUTIERREZ MERCADO\t214700153"<<endl;
	
	do{
		cout<<"\n\t\tCl�nica de Salud �San Juan XXIII�"<<endl;
		cout<<"\n Menu:\n\t 0 - Alta.\n\t 1 - Busqueda.\n\t 2 - Modificar datos.\n\t 3 - Eliminar datos.\n\t 4 - Mostrar empleados entre la edad de [30 - 40].\n\t 5 - Salir.\n\n";
		cout<<" Seleccione una opcion: ";
		cin>>menu;
	
	switch(menu){
		case 0:
			cout<<"\n\n\t 0 - Persona\n\t 1 - Vehiculo\n\n\t Elija una opcion: ";
			cin>>pv;
			cout<<"\n\n";
			system("pause");
			if(pv == 0){
				cout<<"\n\n\t 0 - Paciente\n\t 1 - Administrativo\n\t 2 - Doctor\n\t 3 - Enfermero\n\n\t Elija la opcion que desea dar de alta: ";
				cin>>t;
				altaP(d,t);
			}
			if(pv == 1){
				altaP(d);
			}
			cout<<"\n\n";
			system("pause");
			system("cls")
			d++;
			break;
		case 1:
			busq();
			break;
		case 2:
			modif();
			break;
		case 3:
			borrar();
			break;
		case 4:
			mostrar();
			break;
		default:
			cout<<" Opcion incorrecta, por favor ingrese una del menu.\n\n";
			system("pause");
			system("cls");
			break;
	}
		}while(menu!=5);
	return 0;
}

void altaP(int d, t){
	
	int ec=0;
	
	cout<<"\t\t BIENVENIDO AL MENU DE ALTA (PERSONAS)\n\n";
	
	cout<<" Ingresa el nombre: ";
    cin>>listaP[d].nombre;
	cout<<" Ingresa el apellido paterno: ";
    cin>>listaP[d].apellPat;
    cout<<" Ingresa el apellido materno: ";
    cin>>listaP[d].apellMat;
    cout<<" Ingresa la fecha de nacimiento DDMMAAAA (Ejemplo: 12111998): ";
    cin>>listaP[d].fechaNac;
    listaP[d].edad = 2018-((listaP[d].fechaNac)%10000);
    listaP[d].fechaA = time(&rawtime);
    cout<<" Ingresa la CURP: ";
    cin>>listaP[d].curp;
    cout<<" Ingresa el RFC: ";
    cin>>listaP[d].rfc;
    cout<<" Ingresa el tipo de sangre (Ejemplo: O+): ";
    cin>>listaP[d].sangre;
    //Paciente
	if(t == 0){
		cout<<" Ingresa el padecimiento: ";
    	cin>>listaP[d].padec;
    	
    	cout<<" Padece de una enfermedad cronica\? Si = 1 , No = Cualquier tecla";
    	cin>>ec;
    	if(ec == 1){
    		cout<<" Ingresa el nombre de la enfermedad: ";
    		cin>>listaP[d].enferm;
    		cout<<" Ingrese una breve descripcion de la enfermedad: ";
    		cin>>listaP[d].descrip;
		}
		else{
			goto a;
		}
	}
	//Administrativo
	if(t == 1){
		cout<<" Ingresa el area de administracion: ";
    	cin>>listaP[d].area;
	}
	//Doctor
	if(t == 2){
		cout<<" Ingrese la especialidad: ";
    	cin>>listaP[d].espec;
    	cout<<" Ingrese el numero del consultorio: ";
    	cin>>listaP[d].consul;
	}
	//Enfermero
	if(t == 3){
		cout<<" Ingrese el nombre del area de trabajo: ";
    	cin>>listaP[d].areaEnfer;
	}
	
a:
    return;
}

void altaV(int d, t){
	
	cout<<"\t\t BIENVENIDO AL MENU DE ALTA (VEHICULOS)\n\n";
	
	
	
}
int search(){
	cout<<"Busqueda";
	system("pause");
	system("cls");
	return 1;
}
void update(){
	cout<<"Modificar";
	system("pause");
	system("cls");
	return 1;
}
void erase(){
	cout<<"Eliminar";
	system("pause");
	system("cls");
	return 1;
}
void show(){
	cout<<"Mostrar";
	system("pause");
	system("cls");
	return 1;
}
