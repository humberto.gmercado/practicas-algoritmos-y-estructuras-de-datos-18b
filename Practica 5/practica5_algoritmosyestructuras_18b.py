"""		Práctica 5 - Programe solo con Python (uso de listas)
La empresa logística llamada “Logistic Beach”, se encarga de entregar camiones con carga de mariscos, 
por lo cual, sus operaciones están destinadas en abastecer a toda la zona costera del pacifico de México, 
con esto se logra obtener un listado de la calificación que da el cliente a cada camión de transporte, 
siendo un total de 20 viajes por mes los que realiza cada camión y con esto se obtienen 20 calificaciones, 
estas calificaciones se encuentran en el archivo CSV llamado prac5-valores.csv. con estos datos, 
realice lo siguiente:

	Lea el archivo y genere una lista a la cual se puedan ir incrementando los datos, pues el archivo tiene 
	solo 1,000 camiones y nosotros añadiremos más.

Muestre un menú con las siguientes opciones:
o   Leer archivo
o   Modificar resultados de un viaje de camión. Al terminar de capturar debe guardar todo el contenido 
	de la lista en el archivo.
o   Modificar resultados de los 20 viajes del camión. Al terminar de capturar debe guardar todo el contenido 
	de la lista en el archivo.
o   Buscar camión.Debe mostrar registro
o   Obtener promedios. Realiza el cálculo del promedio de los viajes de cada camión y los guarda en un archivo 
	llamado promedio-camion.csv
o   Salir.

	Al buscar un camión, el usuario debe escribir el id(clave) y el sistema deberá indicar la posición en la 
	que se encuentra este camión, posteriormente debe preguntar en esta misma opción a) si deseo eliminar el 
	registro y b) si deseamos regresar al menú principal."""

from io import open 
import os

camiones=[]

class camion:
	name=""
	pw=""
	cal=[]

def clear_p():
	input()
	os.system("cls")

def lectura():
	archivo=open("prac5-valores.csv","r")
	print(" El archivo se leyo correctamente.")
	input()

	datos=archivo.read().splitlines()
	datos.pop(0)
	for line in datos:
		dats=line.split(',')
		new=camion()
		new.pw=dats[0]
		new.name=dats[1]
		dats.pop(0)
		dats.pop(0)
		new.cal=dats
		camiones.append(new)
	archivo.close()
	print(" Datos procesados.")
	clear_p()
	
def buscar(datos):
	p=input("Ingrese el id del camion (ejemplo id5): ")
	for x in datos:
		if x.pw == p:
			print("Nombre: "+x.name)
			print("Calificaciones:")
			print(x.cal)
			sn=int(input("Desea eliminar el registro (Si = 1 // No = 0): "))
			if sn == 1:
				datos.pop(datos.index(x))
				sobreescribir(datos)
	clear_p()

def modviaje(datos):
	p=input("Ingrese el id del camion (ejemplo id5): ")
	for x in datos:
		if x.pw == p:
			v=int(input("Ingrese el No. del viaje que desea modificar: "))
			print("Viaje: ",x.cal[v-1])
			nv=input("\n Introduce el nuevo valor: ")
			x.cal[v-1]=nv
			sobreescribir(datos)
	clear_p()

def modviajes(datos):
	nViajes=[]
	p=input("Ingrese el id del camion (ejemplo id5): ")
	for x in datos:
		if x.pw == p:
			for y in range(0,20):
				print("Introduce el valor del viaje No. ", y+1)
				valor=input(" : ")
				nViajes.append(valor)
				x.cal[y]=nViajes[y]
	sobreescribir(datos)
	clear_p()

def promedio(datos):
	promC=[]
	valor=0
	for x in datos:
		for y in range (0,20):
			valor = valor + int(x.cal[y])
		prom=valor/20
		promC.append(prom)
		valor = 0
	with open("promedio-camion.csv", "w") as archivo:
		archivo.write("LOGISTIC BEACH\nPROMEDIOS DE LOS CAMIONES\n\nNO. CAMION,PROMEDIO\n")
		for z in range (0, len(datos)):
			num=str(z+1)
			archivo.write(num)
			archivo.write(",")
			archivo.write(str(promC[z]))
			archivo.write("\n")
		archivo.close()
	print("Se ha creado un archivo con el nombre de 'promedio-camion.csv'.")
	clear_p()

def sobreescribir(datos):
	with open("prac5-valores.csv","w") as archivo:
		archivo.write("id,nombreCamion,calViaje1,calViaje2,calViaje3,calViaje4,calViaje5,calViaje6,calViaje7,calViaje8,calViaje9,calViaje10,calViaje11,calViaje12,calViaje13,calViaje14,calViaje15,calViaje16,calViaje17,calViaje18,calViaje19,calViaje20\n")
		for x in datos:
			v = ""
			for y in x.cal:
				v = v + "," + str(y)
			archivo.write(str(x.pw)+","+str(x.name)+str(v)+"\n")
	archivo.close()
	print("Cambios realizados exitosamente.")

print(" H0606 ALGORITMOS Y ESTRUCTURAS DE DATOS\n HUMBERTO ALEJANDRO GUTIERREZ MERCADO\tCODIGO: 214700153\n")

op=0
verificacion=0
while op != 6:
	print("\t LOGISTIC BEACH")
	print("MENU\n 1 - Leer archivo.\n 2 - Modificar resultados de un viaje de camión.\n 3 - Modificar resultados de los 20 viajes del camión.\n 4 - Buscar camión.\n 5 - Obtener promedios.\n 6 - Salir.\n")
	op=int(input("\t Elije una opcion: "))

	if op == 1:
		lectura()
		verificacion=1
	elif op == 2:
		if verificacion == 0:
			print("Sentimos las molestias, primero tiene que leer el archivo.")
			clear_p()
		else:
			modviaje(camiones)
	elif op == 3:
		if verificacion == 0:
			print("Sentimos las molestias, primero tiene que leer el archivo.")
			clear_p()
		else:
			modviajes(camiones)
	elif op == 4:
		if verificacion == 0:
			print("Sentimos las molestias, primero tiene que leer el archivo.")
			clear_p()
		else:
			buscar(camiones)
	elif op == 5:
		if verificacion == 0:
			print("Sentimos las molestias, primero tiene que leer el archivo.")
			clear_p()
		else:
			promedio(camiones)
	elif op == 6:
		print("Gracias por utilizar nuestro servicio.")
		clear_p()
		break
	else:
		print("Opcion incorrecta, por favor elije una del menu.")
		clear_p()