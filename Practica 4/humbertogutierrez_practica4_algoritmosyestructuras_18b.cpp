/*	Pr�ctica 4

Con base a los resultados obtenidos de la pr�ctica 2, realice los puntos que se solicitan en los siguientes p�rrafos, 
probablemente tenga que reestructurar parte del c�digo, le recomiendo conservar copias de respaldo de cada c�digo.

Genere un men� que de opciones de:
1.      Salir
2       Guardar resultados en archivo
3.      Aplicar ordenamiento X
4.      Aplicar ordenamiento Y
5.      Aplicar ordenamiento Z
6.      Mostrar los 100 n�meros de errores m�s frecuentes
7.      Mostrar los 100 n�meros de errores menos frecuentes
8.      Mostrar el numero promedio de error
9.      N�mero de veces que se present� cada Fase
10.   	Fase con mayor presentaci�n en el proceso

Aplique al menos 3 m�todos de ordenamiento a todos los resultados obtenidos, esto podr� generar un solo arreglo con 
todos los valores, este proceso se efect�a con la memoria din�mica.
La opci�n de Guardar resultados en archivo, debe permitir que el usuario escriba el nombre del archivo, este archivo 
deber� ser un archivo csv, ser� almacenado en la misma ruta del c�digo. Antes de iniciar a guardar los datos, debe 
preguntar que opci�n desea guardar, siendo de la opci�n 3 a la 10 de las que se pueden exportar sus resultados.
Elabore los c�digos necesarios para que se puedan ejecutar cada una de las opciones que se muestran en el men�.
Utilice los m�todos, estructuras y c�digos necesarios para procesar lo solicitado. */

#include <iostream>
#include <cstdlib>
#include <cstring>
#include <fstream>
#define n 10000
#define m 10

using namespace std;

bool lectura();
bool fases(int op);
bool guardar(int op);
bool ordenamientos(int op);
bool frecuencia(int op);

int mtx[n][m], o[n*m], nFrec[n/m], vFrec[n/m], FMR=0, FM=0, FA=0, FE=0, prom=0, fasesR=0, ord=0, fre=0;

main(){
	int menu, op;
	
	system("color F0");
	cout<<" H0606 ALGORITMOS Y ESTRUCTURAS DE DATOS\n HUMBERTO ALEJANDRO GUTIERREZ MERCADO\tCODIGO: 214700153"<<endl;
	
	if(lectura() == false){
		cout<<"\n\t Revisa la ubicacion de tus archivos. Gracias.\n"<<endl;
		system("pause");
		goto x;	
	}
	
	do{
a:		cout<<"\t\t ROBOTIC'S MEX\n"<<endl;
		cout<<" MENU:\n\t 1 - Salir.\n\t 2 - Guardar resultados en archivo.\n\t 3 - Aplicar ordenamiento por BURBUJA.\n\t 4 - Aplicar ordenamiento por INSERCION.\n\t 5 - Aplicar ordenamiento por SELECCION.\n\t 6 - Mostrar los 100 numeros de errores mas frecuentes.\n\t 7 - Mostrar los 100 numeros de errores menos frecuentes.\n\t 8 - Mostrar el numero promedio de error.\n\t 9 - Numero de veces que se presento cada fase.\n\t 10 - Fase con mayor presentacion en el proceso.\n\n";
		cout<<" Elija una opcion: ";
		cin>>menu;
		
		switch(menu){
			case 1:
				break;
			case 2:
				cout<<"\n\t Que opcion del menu deseas guardar\?: ";
				cin>>op;
				if (op>= 3 && op<=10){
					guardar(op);
				}
				else{
					cout<<"\n\t Opcion incorrecta. Por favor, ingrese una del menu.\n"<<endl;
					system("pause");
					system("cls");
					goto a;
				}
				break;
			case 3:
				op=1;
				ordenamientos(op);
				break;
			case 4:
				op=2;
				ordenamientos(op);
				break;
			case 5:
				op=3;
				ordenamientos(op);
				break;
			case 6:
				if(ord == 1){
					op=1;
					frecuencia(op);
				}
				else{
					cout<<"\n\t 1 - BURBUJA. \n\t 2 - INSERCION. \n\t 3 - SELECCION. \n\n Que metodo de ordenamiento desea utilizar\?: ";
					cin>>op;
					switch(op){
						case 1:
							ordenamientos(op);
							break;
						case 2:
							ordenamientos(op);
							break;
						case 3:
							ordenamientos(op);
							break;
						default:
							cout<<"\n\t Opcion incorrecta. Por favor, ingrese una del menu.\n"<<endl;
							system("pause");
							system("cls");
							goto a;
					}
					op=1;
					frecuencia(op);
				}
				break;
			case 7:
				if(ord == 1){
					op=2;
					frecuencia(op);
				}
				else{
					cout<<"\n\t 1 - BURBUJA. \n\t 2 - INSERCION. \n\t 3 - SELECCION. \n\n Que metodo de ordenamiento desea utilizar\?: ";
					cin>>op;
					switch(op){
						case 1:
							ordenamientos(op);
							break;
						case 2:
							ordenamientos(op);
							break;
						case 3:
							ordenamientos(op);
							break;
						default:
							cout<<"\n\t Opcion incorrecta. Por favor, ingrese una del menu.\n"<<endl;
							system("pause");
							system("cls");
							goto a;
					}
					op=2;
					frecuencia(op);
				}
				break;
			case 8:
				cout<<"\n\t El numero promedio de error es: "<<prom<<endl<<endl;
				system("pause");
				system("cls");
				break;
			case 9:
				op=1;
				fases(op);
				break;
			case 10:
				op=2;
				fases(op);
				break;
			default:
				cout<<"\n\t Opcion incorrecta. Por favor, ingrese una del menu.\n"<<endl;
				system("pause");
				system("cls");
				goto a;
		}
	} while(menu != 1);
	
	cout<<"\n\t Gracias por utlizar nuestros servicios.\n"<<endl;
	system("pause");
	
x:	return 0;
}

bool lectura(){
	ifstream datos;
	int a=0, z=0;
	char dat;
	string num[100000];
	string prim;
	
	datos.open("prac1-valores.csv");
	
	if (datos.fail() == true){
		cout<<"\n\t El archivo no se puede leer.\n"<<endl;
		system("pause");
		system("cls");
		return false;
	}
	else {
		cout<<"\n\t El archivo se leyo correctamente.\n"<<endl;
		system("pause");
		
		getline(datos,prim);
		datos.get(dat);
		
		for(int x=0;x<n;x++){
			for(int y=0;y<m;y++){
				a=0;
				while(dat != ','){
					num[(x*10)+y][a]=dat;
					datos.get(dat);
					a++;
					if(dat == '\n'){
						goto z;
					}
				}
z:			char *d = const_cast<char*>(num[(x*10)+y].c_str());
			mtx[x][y] = atoi(d);
			o[z] = atoi(d);
			z+=1;
			prom+=mtx[x][y];
		
			if(mtx[9999][9] == 383){
				goto y;
			}
			datos.get(dat);
			}			
		}
	}
y:	datos.close();
	prom/=100000;
	cout<<"\n\t Datos procesados.\n"<<endl;
	system("pause");
	system("cls");
	return true;
}

bool fases(int op){
	
	if(fasesR==0){
		for(int x=0;x<n;x++){
			for(int y=0;y<m;y++){
				if(mtx[x][y] >= 0 && mtx[x][y] <= 200){
					FMR+=1;
				}
				if(mtx[x][y] >= 201 && mtx[x][y] <= 500){
					FM+=1;
				}
				if(mtx[x][y] >= 501 && mtx[x][y] <= 799){
					FA+=1;
				}
				if(mtx[x][y] >= 800 && mtx[x][y] <= 1000){
					FE+=1;
				}
			}
		}
	fasesR+=1;
	}
	if(fasesR==1){
		if(op==1){
			cout<<"\n\t Fase fallida: "<<FMR<<" veces.";
			cout<<"\n\t Fase en condiciones medias: "<<FM<<" veces.";
			cout<<"\n\t Fase en condiciones altas: "<<FA<<" veces.";
			cout<<"\n\t Fase en condiciones excelentes: "<<FE<<" veces.\n"<<endl;
			system("pause");
			system("cls");
		}
		if(op==2){
			if(FMR>FM && FM>FA && FM>FE){
				cout<<"\n\t La fase con mayor presentacion en el proceso fue: FASE FALLIDA. "<<FMR<<" veces.\n"<<endl;
				system("pause");
				system("cls");
			}
			else if(FM>FA && FM>FE){
				cout<<"\n\t La fase con mayor presentacion en el proceso fue: FASE EN CONDICIONES MEDIAS. "<<FM<<" veces.\n"<<endl;
				system("pause");
				system("cls");
			}
			else if(FA>FE){
				cout<<"\n\t La fase con mayor presentacion en el proceso fue: FASE EN CONDICIONES ALTAS. "<<FA<<" veces.\n"<<endl;
				system("pause");
				system("cls");
			}
			else if(FE>FA){
				cout<<"\n\t La fase con mayor presentacion en el proceso fue: FASE EN CONDICIONES EXCELENTES. "<<FE<<" veces.\n"<<endl;
				system("pause");
				system("cls");
			}
			else if(FMR == FM && FM == FA && FA == FE){
				cout<<"\n\t Todas las fases se presentaron con la misma cantidad. "<<FMR<<" veces cada una.\n"<<endl;
				system("pause");
				system("cls");
			}
		}
	}
	return true;
}

bool ordenamientos(int op){
	int aux=0, p=0, z=0, yn=0, cont=0;
	z=n*m;
if(ord == 0){
	if(op == 1){
		cout<<"\n\t ORDENAMIENTO BURBUJA.\n Espere un momento por favor. . .\n"<<endl;
	
		for(int x=0;x<z;x++){
			for(int y=0;y<z-1;y++){
				if(o[y] > o[y+1]){
					aux = o[y];
					o[y] = o[y+1];
					o[y+1] = aux;
				}
			}
		}
	}
	else if(op == 2){
		cout<<"\n\t ORDENAMIENTO INSERCION.\n Espere un momento por favor. . .\n"<<endl;
	
		for(int i=0;i<z;i++){
			p = i; 
			aux = o[i];
			while((p>0) && (o[p-1] > aux)){
				o[p] = o[p-1];
				p--;
			}
			o[p] = aux;
		}
	}
	else if(op == 3){
		cout<<"\n\t ORDENAMIENTO SELECCION.\n Espere un momento por favor. . .\n"<<endl;

		for(int x=0;x<z;x++){
			p = x;
			for(int y=x+1;y<z;y++){
				if(o[y] < o[p]){
					p = y;
				}
			}
			aux = o[x];
			o[x] = o[p];
			o[p] = aux;
		}
	}
	cout<<"\n\t Quieres ver el ordenamiento en pantalla\? (Si = 1 // No = Cualquier tecla): ";
	cin>>yn;
	if(yn == 1){
b:		system("clear");
		cout<<"\n\t DATOS ORDENADOS\n"<<endl;
		for(int x=0;x<z;x++){
			cout<<"\t"<<o[x];
			cont+=1;
			if(cont == 10){
				cont=0;
				cout<<"\n";
			}
		}
		cout<<"\n\t Regresando a la pantalla principal.\n"<<endl;
		system("pause");
		system("cls");
	}
	else if(yn != 1){
		cout<<"\n\t Regresando a la pantalla principal.\n"<<endl;
		system("pause");
		system("cls");
	}
	ord+=1;
}
else if(ord == 1){
	cout<<"\n\t Ya haz ordenado anteriormente los datos.\n\t Deseas visualizarlos en pantalla de nuevo\? (Si = 1 // No = Cualquier tecla): ";
	cin>>yn;
	if(yn == 1){
		goto b;
	}
	else if(yn != 1){
		cout<<"\n\t Regresando a la pantalla principal.\n"<<endl;
		system("pause");
		system("cls");
	}
}
	return true;
}

bool frecuencia(int op){
	int cont=0, num=0, aux=0, aux2=0, z=0, w=0, p=0;
	w=n*m;

	for(int x=0;x<w;){
		num=o[x];
		while(num == o[x]){
			cont++;
			x++;
		}
		nFrec[z]=num;
		vFrec[z]=cont;
		cont=0;
		z++;
	}
	for(int i=0;i<1000;i++){
		p = i; 
		aux = vFrec[i];
		aux2 = nFrec[i];
		while((p>0) && (vFrec[p-1] > aux)){
			vFrec[p] = vFrec[p-1];
			nFrec[p] = nFrec[p-1];
			p--;
		}
		vFrec[p] = aux;
		nFrec[p] = aux2;
	}
	fre+=1;
	if(op == 1){
		cout<<"\n\t Los 100 numeros de errores mas frecuentes.\n"<<endl;
		system("pause");
		cout<<"\nPos\t\tNum\t\tFrec\n"<<endl;
		for(int g=999;g>=900;g--){
		cout<<cont+1<<".-\t\t"<<nFrec[g]<<"\t\t"<<vFrec[g]<<endl;
		cont++;
		}
	}
	if(op == 2){
		cout<<"\n\t Los 100 numeros de errores menos frecuentes.\n"<<endl;
		system("pause");
		cout<<"\nPos\t\tNum\t\tFrec\n"<<endl;
		for(int g=0;g<100;g++){
		cout<<g+1<<".-\t\t"<<nFrec[g]<<"\t\t"<<vFrec[g]<<endl;
		}
	}
	cout<<"\n\n";
	system("pause");
	system("cls");
	return true;
}

bool guardar(int op){
	ofstream datos;
	string nombre;
	int y=0, cont=0;
	
	switch(op){
		case 3:
			if(ord != 1){
				cout<<"\n\t Sentimos las molestias.\n\t Ejecute el comando en el menu para poder guardarlo.\n"<<endl;
				system("pause");
				system("cls");
				goto a;
			}
			break;
		case 4:
			if(ord != 1){
				cout<<"\n\t Sentimos las molestias.\n\t Ejecute el comando en el menu para poder guardarlo.\n"<<endl;
				system("pause");
				system("cls");
				goto a;
			}
			break;
		case 5:
			if(ord != 1){
				cout<<"\n\t Sentimos las molestias.\n\t Ejecute el comando en el menu para poder guardarlo.\n"<<endl;
				system("pause");
				system("cls");
				goto a;
			}
			break;
		case 6:
			if(fre != 1){
				cout<<"\n\t Sentimos las molestias.\n\t Ejecute el comando en el menu para poder guardarlo.\n"<<endl;
				system("pause");
				system("cls");
				goto a;
			}
			break;
		case 7:
			if(fre != 1){
				cout<<"\n\t Sentimos las molestias.\n\t Ejecute el comando en el menu para poder guardarlo.\n"<<endl;
				system("pause");
				system("cls");
				goto a;
			}
			break;
		case 8:
			break;
		case 9:
			if(fasesR != 1){
				cout<<"\n\t Sentimos las molestias.\n\t Ejecute el comando en el menu para poder guardarlo.\n"<<endl;
				system("pause");
				system("cls");
				goto a;
			}
			break;
		case 10:
			if(fasesR != 1){
				cout<<"\n\t Sentimos las molestias.\n\t Ejecute el comando en el menu para poder guardarlo.\n"<<endl;
				system("pause");
				system("cls");
				goto a;
			}
			break;
	}
	cout<<"\n\t Con que nombre desea guardar el archivo\? (ejemplo.csv): ";
	cin>>nombre;
	datos.open(nombre.c_str());
	system("pause");

	if (datos.fail() == true){
		cout<<"\n\t El archivo no se puede crear.\n"<<endl;
		system("pause");
		system("cls");
		return false;
	}
	else{
		switch(op){
			case 3:
				datos<<"ROBOTIC'S MEX\nORDENAMIENTO BURBUJA\n\n";
				for(int x=0;x<(n*m);x++){
					datos<<o[x]<<",";
					y++;
					if(y == 10){
						datos<<"\n";
						y=0;
					}
				}
				break;
			case 4:
				datos<<"ROBOTIC'S MEX\nORDENAMIENTO INSERCION\n\n";
				for(int x=0;x<(n*m);x++){
					datos<<o[x]<<",";
					y++;
					if(y == 10){
						datos<<"\n";
						y=0;
					}
				}
				break;
			case 5:
				datos<<"ROBOTIC'S MEX\nORDENAMIENTO SELECCION\n\n";
				for(int x=0;x<(n*m);x++){
					datos<<o[x]<<",";
					y++;
					if(y == 10){
						datos<<"\n";
						y=0;
					}
				}
				break;
			case 6:
				datos<<"ROBOTIC'S MEX\nLOS 100 NUMEROS DE ERRORES MAS FRECUENTES\n\n";
				datos<<"POSICION,NUMERO,FRECUENCIA\n";
				for(int g=999;g>=900;g--){
					datos<<cont+1<<".-,"<<nFrec[g]<<","<<vFrec[g]<<"\n";
					cont++;
				}
				break;
			case 7:
				datos<<"ROBOTIC'S MEX\nLOS 100 NUMEROS DE ERRORES MENOS FRECUENTES\n\n";
				datos<<"POSICION,NUMERO,FRECUENCIA\n";
				for(int g=0;g<100;g++){
					datos<<g+1<<".-,"<<nFrec[g]<<","<<vFrec[g]<<"\n";
				}
				break;
			case 8:
				datos<<"ROBOTIC'S MEX\nNUMERO PROMEDIO DE ERROR\n\n";
				datos<<"Promedio,=,"<<prom;
				break;
			case 9:
				datos<<"ROBOTIC'S MEX\nNUMERO DE CADA FASE\n\n";
				datos<<"Fase fallida: "<<FMR<<" veces.\n";
				datos<<"Fase en condiciones medias: "<<FM<<" veces.\n";
				datos<<"Fase en condiciones altas: "<<FA<<" veces.\n";
				datos<<"Fase en condiciones excelentes: "<<FE<<" veces.\n"<<endl;
				break;
			case 10:
				datos<<"ROBOTIC'S MEX\nFASE CON MAYOR PRESENCIA\n\n";
				if(FMR>FM && FM>FA && FM>FE){
					datos<<"La fase con mayor presentacion en el proceso fue: FASE FALLIDA. "<<FMR<<" veces.\n";
				}
				else if(FM>FA && FM>FE){
					datos<<"La fase con mayor presentacion en el proceso fue: FASE EN CONDICIONES MEDIAS. "<<FM<<" veces.\n";
				}
				else if(FA>FE){
					datos<<"La fase con mayor presentacion en el proceso fue: FASE EN CONDICIONES ALTAS. "<<FA<<" veces.\n";
				}
				else if(FE>FA){
					datos<<"La fase con mayor presentacion en el proceso fue: FASE EN CONDICIONES EXCELENTES. "<<FE<<" veces.\n";
				}
				else if(FMR == FM && FM == FA && FA == FE){
					datos<<"Todas las fases se presentaron con la misma cantidad. "<<FMR<<" veces cada una.\n"<<endl;
				}
				break;
		}
	}
	cout<<"\n\t Archivo creado correctamente.\n"<<endl;
	system("pause");
a:	system("cls");
	return true;
}
