/*
	Pr�ctica #2
La empresa Robotic�s Mex, realiza un proceso industrial en el cual un robot ejecuta el viaje en las 10 estaciones de la planta, 
en cada viaje se transporta un producto, el cual se va modificando en cada estaci�n, por lo cual, cada estaci�n le informa con 
un numero sobre los resultados del proceso, este n�mero puede ser entre 0 o 1000, por lo cual deber� leer todos los valores que 
fueron entregados en cada estaci�n durante el ciclo de trabajo de 8 horas, dicho ciclo abarca 10,000 operaciones, por lo tanto, 
se le entrega un archivo CSV donde se tienen almacenados los valores de cada vuelta del proceso.

Cada valor tiene un significado con base a este rango

�         0   a 200 = Fase fallida, debe canalizarse a material reciclado = FMR
�         201 a 500 = Fase en condiciones medias = FM
�         501 a 799 = Fase en condiciones altas = FA
�         800 a 1000 = Fase en condiciones excelentes con = FE

Genere un archivo nuevo en el cual indique el resultado de cada fase. 
Utilice arreglos para realizar las comparaciones, es decir, primero debe leer todo el archivo y guardarlo en un arreglo, para 
despu�s generar las comparaciones y almacenar en un nuevo archivo. 


	Producto 1
F1		F2		F3		F4		F5		F6		F7		F8		F9		F10

FMR		FMR		FM		FA		FE		FE		FA		FMR		FA		FE

	Producto 2
*/

#include <iostream>
#include <cstdlib>
#include <fstream>
#define n 10000
#define m 10

using namespace std;

bool lectura();
bool ordenamiento();

int mtx[n][m];

main(){
	system("color F0");
	cout<<" H0606 ALGORITMOS Y ESTRUCTURAS DE DATOS\n HUMBERTO ALEJANDRO GUTIERREZ MERCADO\tCODIGO: 214700153"<<endl;
	if(lectura() == false){
		cout<<"\n\t Revisa la ubicacion de tus archivos. Gracias.\n"<<endl;
		system("pause");
		goto x;
		
	}
	if(ordenamiento() == false){
		cout<<"\n\t Lamentamos la molestia.\n"<<endl;
		system("pause");
		goto x;
	}
	else{
		cout<<"\n\t Los datos han sido clasificados exitosamente.\n"<<endl;
		system("pause");
	}
x:	return 0;
}

bool lectura(){
	ifstream datos;
	int a=0;
	char dat;
	string num[100000];
	string prim;
	
	datos.open("prac1-valores.csv");
	
	if (datos.fail() == true){
		cout<<"\n\t El archivo no se puede leer.\n"<<endl;
		system("pause");
		system("cls");
		return false;
	}
	else {
		cout<<"\n\t El archivo se leyo correctamente.\n"<<endl;
		system("pause");
		
		getline(datos,prim);
		datos.get(dat);
		
		for(int x=0;x<n;x++){
			for(int y=0;y<m;y++){
				a=0;
				while(dat != ','){
					num[(x*10)+y][a]=dat;
					datos.get(dat);
					a++;
					if(dat == '\n'){
						goto z;
					}
				}
z:			char *d = const_cast<char*>(num[(x*10)+y].c_str());
			mtx[x][y] = atoi(d);
		
			if(mtx[9999][9] == 383){
				goto y;
			}
			datos.get(dat);
			}				
		}
	}
y:	datos.close();
	cout<<"\n\t Datos procesados.\n"<<endl;
	system("pause");
	return true;
}

bool ordenamiento(){
	ofstream datos;
	
	datos.open("datos-ordenados_practica2.csv");
	
	if (datos.fail() == true){
		cout<<"\n\t El archivo no se puede crear.\n"<<endl;
		system("pause");
		system("cls");
		return false;
	}
	else{
		cout<<"\n\t El archivo se ha creado correctamente.\n\t Ahora comenzara a ordenarse.\n"<<endl;
		system("pause");
		
		for(int x=0;x<n;x++){
			datos<<"Producto No. "<<x+1<<"\n";
			datos<<"F01,F02,F03,F04,F05,F06,F07,F08,F09,F10\n";
			for(int y=0;y<m;y++){
				if(mtx[x][y] >= 0 && mtx[x][y] <= 200){
				datos<<"FMR,";
				}
				if(mtx[x][y] >= 201 && mtx[x][y] <= 500){
				datos<<"FM,";
				}
				if(mtx[x][y] >= 501 && mtx[x][y] <= 799){
				datos<<"FA,";
				}
				if(mtx[x][y] >= 800 && mtx[x][y] <= 1000){
				datos<<"FE,";
				}
			}
			datos<<"\n\n";
		}
	}
	datos.close();
	return true;
}
