"""	Práctica #2
La empresa Robotic’s Mex, realiza un proceso industrial en el cual un robot ejecuta el viaje en las 10 estaciones de la planta, 
en cada viaje se transporta un producto, el cual se va modificando en cada estación, por lo cual, cada estación le informa con 
un numero sobre los resultados del proceso, este número puede ser entre 0 o 1000, por lo cual deberá leer todos los valores que 
fueron entregados en cada estación durante el ciclo de trabajo de 8 horas, dicho ciclo abarca 10,000 operaciones, por lo tanto, 
se le entrega un archivo CSV donde se tienen almacenados los valores de cada vuelta del proceso.

Cada valor tiene un significado con base a este rango

·         0   a 200 = Fase fallida, debe canalizarse a material reciclado = FMR
·         201 a 500 = Fase en condiciones medias = FM
·         501 a 799 = Fase en condiciones altas = FA
·         800 a 1000 = Fase en condiciones excelentes con = FE

Genere un archivo nuevo en el cual indique el resultado de cada fase. 
Utilice arreglos para realizar las comparaciones, es decir, primero debe leer todo el archivo y guardarlo en un arreglo, para 
después generar las comparaciones y almacenar en un nuevo archivo. 

	Producto 1
F1		F2		F3		F4		F5		F6		F7		F8		F9		F10
FMR		FMR		FM		FA		FE		FE		FA		FMR		FA		FE

	Producto 2

"""
from io import open

def lectura():
	lineas=[]
	enteros=[]

	archivo=open("prac1-valores.csv","r")
	print(" El archivo se leyo correctamente.")
	input()

	primLinea=archivo.readline()
	datos=archivo.readlines()
	for line in datos:
		for x in range (0,1):
			dat=line.split('\n')[x]
			lineas.append(dat)
	for comas in lineas:
		for y in range (0,10):
			dats=comas.split(',')[y]
			enteros.append(int(dats))
	archivo.close()
	print(" Datos procesados.")
	input()
	return enteros

def ordenamiento(datos):
	FMR = 200
	FM = 500
	FA = 799
	FE = 1000

	final=open("datos-ordenados_practica2_python.csv","w")
	print(" El archivo se ha creado correctamente.\n Ahora comenzara a ordenarse.")
	input()

	for x in range(0,10000):
		final.write("Producto No. ")
		num=str(x+1)
		final.write(num)
		final.write("\n")
		final.write("F01,F02,F03,F04,F05,F06,F07,F08,F09,F10,\n")
		z=x*10
		for y in range(0,10):
			p=y+z
			if datos[p] <= FMR:
				final.write("FMR,")
			elif datos[p] <= FM:
				final.write("FM,")
			elif datos[p] <= FA:
				final.write("FA,")
			elif datos[p] <= FE:
				final.write("FE,")
		final.write("\n\n")
	final.close()

print(" H0606 ALGORITMOS Y ESTRUCTURAS DE DATOS\n HUMBERTO ALEJANDRO GUTIERREZ MERCADO\tCODIGO: 214700153\n")

datos=lectura()
ordenamiento(datos)

print(" Los datos han sido clasificados exitosamente.")
input()